#!/bin/bash
export DISPLAY=:0
sudo killall ibus-daemon
echo "stream url: $1"
while true; do 
  ffplay -probesize 32 -analyzeduration 0 -sync ext -fs -fflags nobuffer -flags low_delay -vf setpts=0 -framedrop -avioflags direct -autoexit -loglevel quiet -rtsp_transport tcp $1
  echo "disconnected";
  sleep 5;
done