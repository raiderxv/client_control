import json
import os
import subprocess
import requests
import controls as ctrl
from communication import Communication
import setting as s
import ctypes
# from Queue import Queue
from flask import Flask, session, redirect, url_for, escape, request, jsonify, render_template
from flask_cors import CORS

com =  Communication()
app = Flask(__name__)
CORS(app)

app.secret_key = 'idontcaremuchonthiskey'
app.config['SESSION_TYPE'] = 'memcached'
 
def get_device_id():
    device_id = s.device_id()
    server_url = s.server_url()
    api_url = f'{s.protocol()}://{server_url}/api/v1/devices/deviceid.json'
    print(api_url)
    if (server_url != "" or server_url is not None) and (device_id is None or device_id == ""):
        try:
            result = requests.get(api_url)
            data = result.json()
            device_id = data['deviceid']
            s.update({"device_id": device_id})
            com.disconnect()
        except:
            print("Something went wrong while getting device id.")
    return device_id


@app.route("/")
def index():
    if 'username' in session:
        config = s.fetch()
        return render_template('settings.html', config=config)
    else:
        return redirect(url_for('login'))

@app.route("/device", methods=["POST", "GET"])
def device():
    if request.method == 'POST':
        device_id = request.values.get('device_id')
        if device_id is not None:
            if device_id != s.get("device_id"):
                s.update({"device_id": device_id})
                com.disconnect()
            return jsonify({"success": True, "message": "Device id save."})
        else:
            return jsonify({"success": False, "error": "Device id is required."})
    else:
        setting = s.fetch()
        return jsonify({"device_id": setting["device_id"]})
        

# Save settinguration setting
@app.route("/settings", methods=['POST', 'GET'])
def setting():
    if request.method == 'POST' and 'username' in session:
        params = request.form
        server_url = params.get("server_url")
        player_url = params.get("player_url")

        if s.get("server_url") != server_url:
            get_device_id()

        s.update({"server_url": server_url,"player_url": player_url})
        return render_template('settings.html', message="setting saved.", config=s.fetch())
    else:
        return redirect(url_for('index'))

@app.route("/login", methods=['GET', 'POST'])
def login():
    if request.method == 'POST':
        config = s.fetch()
        username = request.form.get('username')
        password = request.form.get('password')
        if username == config["username"] and password == config["password"]:
            session['username'] = username
            return redirect(url_for('index'))
        else:
            return render_template('login.html', error='Username or password did not match.')
    else:
        return render_template('login.html')

@app.route("/logout")
def logout():
    session.pop('username', None)
    return redirect(url_for('login'))



if __name__ == "__main__":
    try:
        s.get_remote_config()
        ctrl.run_player()
        com.connect()
        app.run(host="0.0.0.0", port=8080, debug=False)
    except KeyboardInterrupt:
        ctrl.stop_player()
        com.disconnect()
        print("Exit")