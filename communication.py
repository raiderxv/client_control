import websocket
from threading import Thread
import time
import json
import setting as s
import controls
import os

class Communication(object):
  def __init__(self):
    
    self.protocol = 'wss' if s.get('secure') else 'ws'
    self.server_url = s.get('server_url')
    self.host = f'{self.protocol}://{self.server_url}/cable'
    self.ws = None
    self.thread = None
    self.heartbeat = None
    self.identifier = None

  def connect(self):
    websocket.enableTrace(True)
    serial_number = s.get_serial_no()
    print(serial_number)
    self.identifier = json.dumps({
      "channel": "ControlChannel", 
      "device_id": s.device_id(), 
      "serial_number": serial_number, 
      "device_type": "player"
      })
    self.ws = websocket.WebSocketApp(self.host,
                              on_open=self.on_open,
                              on_message=self.on_message,
                              on_ping=self.on_ping,
                              on_error=self.on_error,
                              on_close=self.on_close)
    self.thread = Thread(target=self.ws.run_forever, args = ())
    self.thread.start()
  
  def disconnect(self):
    self.ws.close()

  def on_message(self, ws, message):
    message = json.loads(message)
    print(message)
    if 'message' in message and type(message["message"]) is dict:
      data = message["message"]
      if 'action' in data and hasattr(controls, data["action"]):
        self.current_action = data['action'] #ensure not to duplicate actions
        action = getattr(controls, data['action'])
        action()
    
  def on_error(self, ws, error):
    print(error)

  def on_ping(self, ws, message):
    print("GOT PING")

  def on_close(self, ws, close_status_code, close_msg):
    for i in range(15):
      print(f'RECONNECTING IN {15 - i} seconds.....')
      time.sleep(1)
    
    self.connect()
    print("### closed ###")

  def perform(self, action, data=dict):
    data["action"] = action
    self.send(data)
  
  def send(self, data):
    self.ws.send(json.dumps({"command": "message", "identifier": self.identifier, "data": json.dumps(data)}))

  def on_open(self, ws):
    command = {"command": "subscribe", "identifier": self.identifier}
    self.ws.send(json.dumps(command))

    def run(*args):
      while True:
        time.sleep(15)
        print("beating...")
    self.heartbeat = Thread(target=run, args=())
    self.heartbeat.start()