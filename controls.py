import os
import time
import setting as s
import app_root
from screeninfo import get_monitors

def reboot():
  os.system('sudo reboot')

def poweroff():
  os.system('sudo poweroff')

def refresh():
  reload_player()
  print("REFRESH TRIGGERED.................")

def get_browser_flags():
  browser = s.get("browser")
  defualt_flag = "--kiosk"
  if browser in ["chromium-browser", "google-chrome"]:
    return "--kiosk --noerrdialogs --autoplay-policy=no-user-gesture-required --password-store=basic"
  else:
    return defualt_flag

def run_signage():
  time.sleep(15)
  # signage_sh = os.path.json(app_root.path, 'signage.sh')
  player_url = s.get("player_url")
  device_id = s.get('device_id')
  protocol = 'https' if s.get("secure") else 'http'
  full_player_url = f'{protocol}://{player_url}'
  flags =  get_browser_flags()
  os.system(f'{s.get("browser")} {flags} {full_player_url} &')


def run_videowall():
  time.sleep(3)
  stream_url = s.get('stream_url')
  if stream_url is not None:
    videowall_sh = os.path.join(app_root.path, 'videowall.sh')
    os.system(f'sh {videowall_sh} {stream_url} &')
    

def kill_signage():
  browser_proc = "chrome" if s.get('browser') is "google-chrome" else s.get('browser')
  os.system(f'pkill -f {browser_proc}')

def kill_videowall():
  os.system("sudo killall ffplay")
  os.system("sudo pkill -f videowall")

def player():
  return s.get('player_type')

def reload_player():
  s.get_remote_config()
  stop_player()
  if player() == 'videowall':
    run_videowall()
  else:
    run_signage()
  print(f'{player()} has been reloaded')


def stop_player():
  kill_signage()
  kill_videowall()

def run_player(delay=0):
  time.sleep(delay)
  reload_player()

def activate_videowall():
  update_player("videowall")

def activate_signage():
  update_player("signage")

def update_player(player_type):
  s.update({"player_type": player_type})
  reload_player()

def get_monitor():
  try:
    monitor = get_monitors[0]
    payload = {
      "width": monitor.width,
      "height": monitor.height,
      "width_mm": monitor.width_mm,
      "height_mm": monitor.height_mm
    }

    device_id = s.device_id()
    server_url = s.server_url()
    api_url = f'{s.protocol()}://{server_url}/api/v1/devices/{device_id}.json'
    try:
      result = requests.post(api_url, data=payload)
      data = result.json()
    except:
      print("Something went wrong while sending monitor information.")
  except:
    return 'no monitor'

def show_loading():
  if player() == 'videowall':
    reload_player()
    
    
