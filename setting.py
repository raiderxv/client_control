import json
import os
import app_root
import subprocess
import requests

config_path = os.path.join(app_root.path, 'config.json')

def get_remote_config():
  api_url = f'{protocol()}://{server_url()}/signage/api/devices/{device_id()}.json'
  print(api_url)
  try:
    result = requests.get(api_url)
    data = result.json()
    print(data)
    if data['success']:
      update(data['data'])
  except:
      print("Something went wrong while getting stream url.")
  

def fetch():
  config_file = open(config_path)
  return json.load(config_file)

def save(config):
  with open(config_path, 'w', encoding='utf-8') as f:
    json.dump(config, f, ensure_ascii=False, indent=4)

def update(data):
  settings = fetch()
  for key, value in data.items():
    settings[key] = value
  save(settings)

def protocol():
  p = 'https' if get("secure") else 'http'
  return p

def server_url():
  return get("server_url")

def player_url():
  return get("player_url")

def device_id():
  did = get("device_id")
  if not did :
    machine_id = open("/etc/machine-id", "r").read()
    return machine_id
  else:
    return did

def get_serial_no():
    # FOR STREAMER: sudo dmidecode -t system | grep UUID

    getVersion = subprocess.Popen("cat /proc/device-tree/serial-number", shell=True, stdout=subprocess.PIPE).stdout
    version = getVersion.read()
    return version.decode()

def get(key):
  settings = fetch()
  try: 
    return settings[key]
  except:
    return 'Unknown config.'